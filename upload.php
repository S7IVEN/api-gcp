<?php

require_once __DIR__ . '/vendor/autoload.php';

use Google\Cloud\Storage\StorageClient;


/**
* Upload a file.
*
* @param string $bucketName the name of your Google Cloud bucket.
* @param string $objectName the name of the object.
* @param string $source the path to the file to upload.
*
* @return Psr\Http\Message\StreamInterface
*/
function upload_object($bucketName, $objectName, $source)
{
	$storage = new StorageClient();
	$file = fopen($source, 'r');
	$bucket = $storage->bucket($bucketName);
	$object = $bucket->upload($file, [
	'name' => $objectName
	]);
	printf('Uploaded %s to gs://%s/%s' . PHP_EOL, basename($source), $bucketName, $objectName);
}
putenv('GOOGLE_APPLICATION_CREDENTIALS=./credentials.json');

upload_object("ocr-test-delphi", "input.pdf", "input.pdf");